const { sequelize } = require("./models");

// litreally sync models to db

async function main() {
	await sequelize.sync({ force: true });
}

main();
