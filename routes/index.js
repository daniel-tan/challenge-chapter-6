var express = require("express");
// const session = require("express-session");
var router = express.Router();
const fs = require("fs");
const auth_path = "./auth.json";

/* GET home page. */
router.get("/", function (req, res) {
	res.render("index", {
		message: "welcome",
		status: "success",
	});
});

router.post("/login", (req, res) => {
	const { id, pass } = req.body;
	fs.readFile(auth_path, "utf8", (err, data) => {
		if (err) {
			res.render("index", {
				message: "error , " + err.message,
				status: "danger",
			});
		} else {
			const auth = JSON.parse(data);
			if (auth.id === id && auth.password === pass) {
				req.session.loggedIn = true;
				res.redirect("admin");
			} else {
				res.render("index", {
					message: "Wrong id or password please try again",
					status: "danger",
				});
			}
		}
	});
});

router.get("/logout", (req, res) => {
	req.session.destroy();
	res.render("index", {
		message: "You have been logged out",
		status: "success",
	});
});

module.exports = router;
