var createError = require("http-errors");
var flash = require("connect-flash");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
const session = require("express-session");
var logger = require("morgan");
var methodOverride = require("method-override");

var indexRouter = require("./routes/index");
var adminRouter = require("./routes/admin");
let biodataRouter = require("./routes/biodata");

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views/pages"));
app.set("view engine", "ejs");
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(flash());
const oneHour = 1000 * 60 * 60;
app.use(
	session({
		secret: "danieltan 123",
		resave: false, //? resave session variables?
		saveUninitialized: false, //? bisa simpan data kosong?
		name: "uniqueSessionID",
		cookie: {
			maxAge: oneHour,
		},
	})
);
app.use(express.static(path.join(__dirname, "public")));
app.use(methodOverride("_method"));

// router
app.use("/", indexRouter);
app.use("/admin", adminRouter);
app.use("/biodata", biodataRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get("env") === "development" ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.render("error");
});

module.exports = app;
