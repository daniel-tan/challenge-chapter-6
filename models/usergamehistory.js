"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class userGameHistory extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate({ userGame }) {
			// define association here
			userGameHistory.belongsTo(userGame, {
				foreignKey: "userId",
			});
		}
	}
	userGameHistory.init(
		{
			historyId: {
				type: DataTypes.INTEGER,
				primaryKey: true,
				autoIncrement: true,
			},
			userId: {
				type: DataTypes.STRING,
				tableName: "user_id",
			},
			result: DataTypes.STRING,
		},
		{
			sequelize,
			modelName: "userGameHistory",
			underscored: true,
		}
	);
	return userGameHistory;
};
